package raft

import (
	"sync"
)

// Storage is an interface implemented by stable storage providers.
type Storage interface {
	Set(key string, value []byte)
	Get(key string) ([]byte, bool)

	// HasData returns true if any Sets were made on this Storage.
	HasData() bool
}

// MapStorage is an implementation of the Storage interface that uses in-memory map.
type MapStorage struct {
	mu sync.Mutex
	m  map[string][]byte
}

// NewMapStorage creates a new instance of MapStorage.
func NewMapStorage() *MapStorage {
	return &MapStorage{
		mu: sync.Mutex{},
		m:  make(map[string][]byte),
	}
}

// Set will save the specified value inside of the map with the specified key.
func (ms *MapStorage) Set(key string, value []byte) {
	ms.mu.Lock()
	defer ms.mu.Unlock()
	ms.m[key] = value
}

// Get gets the value from the map. If the key doesn't exist it will return
// false.
func (ms *MapStorage) Get(key string) ([]byte, bool) {
	ms.mu.Lock()
	defer ms.mu.Unlock()
	v, found := ms.m[key]
	return v, found
}

// HasData check if there is any data in the map.
func (ms *MapStorage) HasData() bool {
	ms.mu.Lock()
	defer ms.mu.Unlock()
	return len(ms.m) > 0
}
